#!/bin/sh
set -e

mkdir build
cd build
cmake .. \
		-DQNNPACK_BUILD_BENCHMARKS=OFF \
		-DCLOG_SOURCE_DIR=/nonexistent \
		-DCPUINFO_SOURCE_DIR=/nonexistent \
		-DFP16_SOURCE_DIR=/nonexistent \
		-DFXDIV_SOURCE_DIR=/nonexistent \
		-DPSIMD_SOURCE_DIR=/nonexistent \
		-DPTHREADPOOL_SOURCE_DIR=/nonexistent \
		-DGOOGLETEST_SOURCE_DIR=/nonexistent
make
make test
cd ..
rm -rf build
